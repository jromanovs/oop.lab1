﻿using System;
namespace tsi.oop._2021.lab1
{
    public class Product
    {
        public Product(string sku, string name)
        {
            Sku = sku;
            Name = name;
        }

        public override string ToString()
        {
            return $"{Sku,-10} {Name,-35}";
        }

        public string Name { get; set; }

        public string Sku { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;

namespace tsi.oop._2021.lab1
{
    public class StockItem
    {
        public StockItem(Product product, int quantity, decimal price)
        {
            Product = product;
            Quantity = quantity;
            Price = price;
        }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public Product Product { get; set; }

        public decimal Total => Quantity * Price;

        public override string ToString()
        {
            return $"{Product,-47} {Quantity,-5} x {Price,-10:C2} = {Total,-10:C2}";
        }

    }

    public class Warehouse
    {
        public Warehouse()
        {
            Items = new List<StockItem>();
        }

        public List<StockItem> Items { get; set; }

        public int MaximumQuantity { get; set; } = 0;

        public void AddProduct(Product product, int quantity, decimal price)
        {
            Items.Add(new StockItem(product, quantity, price));
            if (quantity>MaximumQuantity)
            {
                MaximumQuantity = quantity;
            }
        }

        public IEnumerable<StockItem> GetStatistics(int limit = 5)
        {
            foreach(StockItem e in Items)
            {
                if (e.Quantity <= limit || e.Quantity == MaximumQuantity)
                {
                    yield return e;
                }
            }
        }
    }
}

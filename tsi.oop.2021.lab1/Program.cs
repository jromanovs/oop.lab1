﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

// Task:
// Сreate a console app for the warehouse. the application must implement at least 3 functions:
//    add information about a new product. (at least 3 fields)
//    show all products and their quantities
//    statistics - show which products are running out and which are the most.
// upload the archive with the application to the system.

// Bitbucket repository:
// git clone https://jromanovs@bitbucket.org/jromanovs/oop.lab1.git 

namespace tsi.oop._2021.lab1
{
    class Program
    {
        private const string STORAGE_FILE_NAME = "warehouse.json";

        static void Main(string[] args)
        {
            var running = true;
            var w = LoadFrom(STORAGE_FILE_NAME);

            while (running)
            {
                Console.WriteLine(
                    "\n\nSelect command:\n"
                  + "1) Add a new product\n"
                  + "2) Display all products\n"
                  + "3) Display statistics\n"
                  + "-----------------------\n"
                  + "9) Exit"
                );
                Console.Write("> ");
                var cki = Console.ReadKey();
                Console.WriteLine("\n");
                switch (cki.Key)
                {
                    case ConsoleKey.D1:
                        AskToEnterProduct(w);
                        Save(w, STORAGE_FILE_NAME);
                        break;
                    case ConsoleKey.D2:
                        DisplayAllProducts(w);
                        break;
                    case ConsoleKey.D3:
                        DisplayStatistics(w);
                        break;
                    case ConsoleKey.D9:
                        running = false;
                        break;
                }
            }
            Console.WriteLine("\n\nApplication terminated.\nHave a good day :)");
        }

        private static Warehouse LoadFrom(string file_name)
        {
            if (File.Exists(STORAGE_FILE_NAME))
            {
                var str = File.ReadAllText(STORAGE_FILE_NAME);
                return JsonConvert.DeserializeObject<Warehouse>(str);
            }
            else
            {
                return new Warehouse();
            }
        }

        private static void Save(Warehouse w, string file_name)
        {
            var str = JsonConvert.SerializeObject(w);
            File.WriteAllText(file_name, str);
        }

        private static void AskToEnterProduct(Warehouse w)
        {
            Console.WriteLine("Please eneter a new prodcut:\n");
            var sku = AskForInput("Product SKU (code): ");
            var name = AskForInput("Product name: ");
            var qty = AskForInput("Quantity: ");
            var price = AskForInput("Price: ");
            if (int.TryParse(qty, out int n_qty) &&
                decimal.TryParse(price, out decimal n_price))
            {
                w.AddProduct(new Product(sku, name), n_qty, n_price);
                Console.WriteLine("\nA new product added");
            }
            else
            {
                ShowInRed("\n*ERROR* Bad input detected");
            }

            static string AskForInput(string prompt)
            {
                Console.Write(prompt);
                return Console.ReadLine();
            }
        }

        private static void DisplayAllProducts(Warehouse w)
        {
            decimal total = 0m;
            DisplayHeader("Products available");
            foreach (StockItem e in w.Items)
            {
                Console.WriteLine($"{e}");
                total += e.Total;
            }
            DisplayFooter($"Total: {total,10:C2}");
        }

        private static void DisplayStatistics(Warehouse w)
        {
            DisplayHeader("Small (<=5) and large stock of goods");
            foreach (StockItem e in w.GetStatistics())
            {
                if (e.Quantity <= 5)
                {
                    ShowInRed(e.ToString());
                } else
                {
                    Console.WriteLine(e);
                }
            }
            DisplayFooter();
        }

        private static void DisplayHeader(string title)
        {
            var line = new string('-', 79);
            Console.WriteLine(line);
            Console.WriteLine(title);
            Console.WriteLine(line);
        }

        private static void DisplayFooter(string footer = "")
        {
            var line = new string('-', 79);
            Console.WriteLine(line);
            Console.WriteLine(footer);
        }

        private static void ShowInRed(string text)
        {
            ConsoleColor currentForeground = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = currentForeground;
        }
    }
}
